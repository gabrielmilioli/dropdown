$('.dropdown').on('click', function () {
	if ($(this).hasClass('open')) {
		return;
	} else {
		$('.dropdown').removeClass('open');
		$(this).addClass('open');
		
		window.onclick = function(event){
			if ($(event.target).parents('.dropdown').length || 
				($(event.target).attr('class') && $(event.target).attr('class').includes('dropdown'))) {
				return;
			}

			$('.dropdown').removeClass('open');
			window.onclick = undefined;
		};
	}
});
